#!/usr/bin/env pwsh

# boilerplate
Set-StrictMode -Version Latest
$ErrorActionPreference = "Stop"
$PSNativeCommandUseErrorActionPreference = $true

$7zipPath = "C:\Program Files\PeaZip\res\bin\7z\7z.exe"

# step 1: build packer image
if (Test-Path -Path .\packer_cache) {
    # must not exist
    Remove-Item -Recurse -Force .\packer_cache
}
if (Test-Path -Path .\packer_output) {
    # sometimes causes issues
    Remove-Item -Recurse -Force .\packer_output
}
if (Test-Path -Path .\debian12.box.tmp) {
    # can be left behind if a previous build was aborted
    Remove-Item -Path ".\debian12.box.tmp" 
}

packer init debian12.pkr.hcl
packer build .\debian12.pkr.hcl

# step 2: create vagrant box format from packer output
## remove old boxes
if (Test-Path -Path ".\debian12.box.tmp") {
    Remove-Item ".\debian12.box.tmp"
}
if (Test-Path -Path ".\debian12.box") {
    Remove-Item ".\debian12.box"
}

## gather files into a tar archive and compress them with gzip
& $7zipPath a -ttar ".\debian12.box.tmp" ".\packer_definitions\*" > $null
& $7zipPath a -ttar ".\debian12.box.tmp" ".\packer_output\*" > $null
& $7zipPath a -tgzip ".\debian12.box" ".\debian12.box.tmp"  > $null
Remove-Item -Path ".\debian12.box.tmp" 

