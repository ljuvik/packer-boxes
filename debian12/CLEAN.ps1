#!/usr/bin/env pwsh

# boilerplate
Set-StrictMode -Version Latest
$ErrorActionPreference = "Stop"
$PSNativeCommandUseErrorActionPreference = $true

# step 4: manually verfify

# step 5: remove vm and uninstall box from host 
vagrant destroy
vagrant box remove debian12
