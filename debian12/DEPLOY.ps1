#!/usr/bin/env pwsh

# boilerplate
Set-StrictMode -Version Latest
$ErrorActionPreference = "Stop"
$PSNativeCommandUseErrorActionPreference = $true

# step 3: deploy vagrant box
if (Test-Path -Path .\.vagrant) {
    Remove-Item -Recurse -Force .\.vagrant
    vagrant global-status --prune
    vagrant destroy
    vagrant box remove debian12
}

vagrant box add --name debian12 .\debian12.box
vagrant up
