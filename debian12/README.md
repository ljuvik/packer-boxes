
# Debian 12 (bookworm) on Hyper-V
I needed a few Hyper-V compatible vagrant box. However the generic/robox/lavabit ones looked far to complicated for me to be comfortable using. So, to better understand how they work I made my own.

This is based on my own needs and should be considered experimental. Since I do not need many vagrant features I have not tested them all. However, for my use cases they are fine. Though if your expecting to be able to share files between the host and guest system you will be disappointed.

## TODO
Partitioning is: ESP, root, swap... this is more flexible but perhaps it would be more useful in more labs if it was the default...
  * https://wiki.debian.org/AutomatedInstallation
  * https://www.debian.org/releases/bookworm/example-preseed.txt
  * https://github.com/lavabit/robox/blob/master/http/generic.debian12.vagrant.cfg
  * https://github.com/chef/bento/blob/main/packer_templates/http/debian/preseed.cfg

## How to use the vagrant box
By the end of this guide you should end up with the following directory structure:
```ps1
Get-ChildItem
 Name
 ----
 debian12.box
 Vagrantfile
```

1. Enable `Hyper-V` if you haven't already (this requires elevated privileges):
```ps1
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All
```

2. Install packer, if you're using `chocolatey` you can run the following (this requires elevated privileges):
```ps1
choco install -y packer
```

3. Download the `debian12.box` from the release page and add it to vagrant:
```ps1
vagrant box add --name debian12 .\debian12.box
```

4. Then create your `Vagrantfile` or use the one included in the git repository. Note that the box does not come with `sudo` installed or configured, and it also doesn't support file sharing between the guest and host.

5. To deploy the virtual machine, make sure you're in the same directory as your `Vagrantfile` and run the following (this requires elevated privileges):
```ps1
vagrant up
```

## How to build the vagrant box
1. Enable `Hyper-V` if you haven't already (this requires elevated privileges):
```ps1
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All
```

2. Clone this repository. But make sure you have `git lfs` installed before doing so.

3. Use `chocolatey` to install the build dependencies (this requires elevated privileges):
```ps1
choco install -y \
    packer       \
    vagrant      \
    peazip
```

4. Use `packer` to check the syntax of the packerfile Vagrant uses to build the Vagrantbox:
```ps1
packer validate debian12.pkr.hcl
```

5. Use a debian machine to check the syntax of `preseed.cfg` and `provision.sh`:
```sh
debconf-set-selections -c unattend/preseed.cfg
shellcheck unattend/provision.sh
```

6. Navigate to your cloned repository and run the following scripts in order (this requires elevated privileges).
Note that this from WSL or from a mounted WSL-filesystem, it has to be performed entirely from Windows:
```ps1
.\BUILD.ps1
.\DEPLOY.ps1
```

When running the two above scripts make sure that your host machine does not go into sleep/hibernate as this will likely interrupt the build/deploy process.

Assuming everything worked correctly you should now see a vagrant box in your cloned repository.

