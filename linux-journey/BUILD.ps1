#!/usr/bin/env pwsh

# boilerplate
Set-StrictMode -Version Latest
$ErrorActionPreference = "Stop"
$PSNativeCommandUseErrorActionPreference = $true

$7zipPath = "C:\Program Files\PeaZip\res\bin\7z\7z.exe"

# step 1: build packer image
if (Test-Path -Path .\packer_cache) {
    # must not exist
    Remove-Item -Recurse -Force .\packer_cache
}
if (Test-Path -Path .\packer_output) {
    # sometimes causes issues
    Remove-Item -Recurse -Force .\packer_output
}
if (Test-Path -Path .\linux-journey.box.tmp) {
    # can be left behind if a previous build was aborted
    Remove-Item -Path ".\linux-journey.box.tmp" 
}

packer init linux-journey.pkr.hcl
packer build .\linux-journey.pkr.hcl

# step 2: create vagrant box format from packer output
## remove old boxes
if (Test-Path -Path ".\linux-journey.box.tmp") {
    Remove-Item ".\linux-journey.box.tmp"
}
if (Test-Path -Path ".\linux-journey.box") {
    Remove-Item ".\linux-journey.box"
}

## gather files into a tar archive and compress them with gzip
& $7zipPath a -ttar ".\linux-journey.box.tmp" ".\packer_definitions\*" > $null
& $7zipPath a -ttar ".\linux-journey.box.tmp" ".\packer_output\*" > $null
& $7zipPath a -tgzip ".\linux-journey.box" ".\linux-journey.box.tmp"  > $null
Remove-Item -Path ".\linux-journey.box.tmp" 

