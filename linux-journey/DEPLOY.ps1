#!/usr/bin/env pwsh

# boilerplate
Set-StrictMode -Version Latest
$ErrorActionPreference = "Stop"
$PSNativeCommandUseErrorActionPreference = $true

# step 3: deploy vagrant box
if (Test-Path -Path .\.vagrant) {
    Remove-Item -Recurse -Force .\.vagrant
    vagrant global-status --prune
    vagrant destroy
    vagrant box remove linux-journey
}

vagrant box add --name linux-journey .\linux-journey.box
vagrant up
