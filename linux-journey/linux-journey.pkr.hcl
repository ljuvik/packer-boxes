
packer {
  required_plugins {
    hyperv = {
      source  = "github.com/hashicorp/hyperv"
      version = "~> 1"
    }
  }
}

variable "vm_name" {
  type = string
  default = "linux-journey"
}

variable "cpus" {
  type = number
  default = 1
}

variable "memory" {
  type = number
  default = 1024
}

variable "disk_size" {
  type = number
  default = 15000
}

variable "iso_url" {
  type = string
  default = "https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-12.9.0-amd64-netinst.iso"
}

variable "iso_checksum" {
  type = string
  default = "file:https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/SHA256SUMS"
}

variable "switch_name" {
  type = string
  default = "Default Switch"
}

source "hyperv-iso" "linux-journey" {
  configuration_version = "9.0"
  vm_name = "${var.vm_name}"
  generation = 2
  enable_secure_boot = false
  guest_additions_mode = "disable"

  cpus = "${var.cpus}"
  memory = "${var.memory}"
  enable_dynamic_memory = true
  disk_size = "${var.disk_size}"

  iso_url = "${var.iso_url}"
  iso_checksum = "${var.iso_checksum}"

  switch_name = "${var.switch_name}"

  communicator = "ssh"
  ssh_username = "root"
  ssh_password = "root"
  ssh_timeout = "1h"

  boot_wait = "5s"
  http_directory = "unattend/"
  boot_command = [
    "<esc><wait>c<wait> ",
    "linux /install.amd/vmlinuz auto=true ",
    "preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/preseed.cfg ",
    "priority=critical --- <enter>",
    "initrd /install.amd/initrd.gz<enter>",
    "boot<enter>"
  ]
  shutdown_command = "shutdown -P now"
  output_directory = "packer_output"
}

build {
  sources = [
    "source.hyperv-iso.linux-journey"
  ]
  provisioner "shell" {
    scripts = [
      "unattend/linux-journey.sh",
      "unattend/provisioner.sh"
    ]
    start_retry_timeout = "5m"
    timeout = "30m"
  }
}
