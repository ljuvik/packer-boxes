#!/usr/bin/env bash

# post-installation this script is loaded by Packer using sftp/scp, then when this exits with status 0 Packer stops the machine and exports it
# this script is ran first and then untattend/provisioner.sh

set -eou pipefail
set -x

# boilerplate
[ "$(id -u)" -ne 0 ] && printf 'Provision script must be run as root\n' && exit 1
! command -v sed > /dev/null 2>&1 && printf "Command 'sed' not found, install it and try again\n" && exit 1
! command -v wget > /dev/null 2>&1 && printf "Command 'wget' not found, install it and try again\n" && exit 1

# hostname settings
HOSTNAME="icebox"
echo "$HOSTNAME" > /etc/hostname
sed -i "s/debian-bookworm/$HOSTNAME/g" /etc/hosts

# users and groups
groupadd penguingroup
USERS='pete anna'
for USER in $USERS; do
	useradd "$USER" --user-group --create-home --shell /bin/bash
	echo "$USER:$USER" | chpasswd
	usermod --append --groups penguingroup "$USER"
done

# dirs
## linux journey uses xdg dirs in their examples (plus some extras)
DIRS='Desktop Documents Downloads Music Pictures Public Templates Videos Movies directory1'
for DIR in $DIRS; do
	install --mode 744 --owner pete --group penguingroup --directory "/home/pete/$DIR"
done
DIRS='Hawaii Bahamas Mexico France'
for DIR in $DIRS; do
	install --mode 744 --owner pete --group pete --directory "/home/pete/Pictures/$DIR"
done
install --mode 744 --owner pete --group pete --directory /home/pete/Templates/MyFolder

# files
BANANA='/home/pete/banana.jpg'
wget --quiet 'https://upload.wikimedia.org/wikipedia/commons/f/f4/Banana_-_Q503.jpg' --output-document "$BANANA"
chown pete:penguingroup "$BANANA"

APPLE='/home/pete/apple.jpg'
wget --quiet 'https://upload.wikimedia.org/wikipedia/commons/1/15/Red_Apple.jpg' --output-document "$APPLE"
chown pete:penguingroup "$APPLE"

PUPPIES='/home/anna/puppies.jpg'
wget --quiet 'https://upload.wikimedia.org/wikipedia/commons/6/64/4_Puppies_in_a_basket.jpg' --output-document "$PUPPIES"
chown --recursive anna:penguingroup /home/anna
chmod --recursive 750 /home/anna
chown --recursive anna:penguingroup /home/vagrant
chmod --recursive 750 /home/vagrant

SECRET='/home/pete/Documents/.secret'
echo "Q503" > "$SECRET"
chown pete:pete "$SECRET"

DOGFILE='/home/pete/dogfile'
cat << EOF > "$DOGFILE"
Alaskan husky
Dachshund
English Foxhound
Finnish Lapphund
German Spitz
Greyhound
Rat Terrier
Shetland Sheepdog
EOF
chown pete:penguingroup "$DOGFILE"

BIRDFILE='/home/pete/birdfile'
cat << EOF > "$BIRDFILE"
Bald eagle
Cockatoo
Grey parraot
Humboldt penguin
Mallard
Pigeon
Raven
Toucan
EOF
chown pete:penguingroup "$BIRDFILE"

TEXT1='/home/pete/Documents/text1'
cat << EOF > "$TEXT1"
Lorem ipsum odor amet, consectetuer adipiscing elit. Lectus vitae consectetur vel venenatis amet odio dictumst. Iaculis vivamus lobortis dictum placerat curabitur sollicitudin aptent. Libero taciti taciti arcu sem eleifend metus hac. Natoque etiam suscipit habitasse diam iaculis malesuada, ante molestie. Phasellus scelerisque inceptos morbi senectus fusce neque tempor blandit. Tempor facilisis euismod class; sed ac per. Nulla quis mattis eros felis, platea enim posuere diam praesent. Mi ligula class magnis senectus mus. Ipsum sagittis malesuada, natoque varius imperdiet fermentum ipsum.

Varius himenaeos consequat neque facilisi natoque porttitor maximus. Torquent sociosqu pretium sed ridiculus quam eu nulla ut laoreet. Lobortis proin facilisis sed phasellus nostra. Mollis maecenas eleifend ridiculus tincidunt fusce integer. At dui ullamcorper porttitor vestibulum non nunc vivamus mauris inceptos. Inceptos hendrerit massa taciti primis dignissim nullam lacus quam tortor. Taciti pharetra eros pulvinar; quis quis per.

Enim finibus sapien mattis gravida nam senectus nam elementum congue. Curabitur penatibus efficitur laoreet lobortis per dolor. Orci etiam facilisis potenti commodo leo sollicitudin inceptos suscipit! Fermentum finibus cursus fusce senectus commodo quis suspendisse tortor. Auctor tellus duis fermentum suspendisse suspendisse; nisi facilisi mi fames. Non phasellus felis risus praesent accumsan dignissim. Dis at ultrices semper; leo arcu magna. Pulvinar hendrerit sollicitudin suscipit ullamcorper ante primis condimentum cubilia.

Sem dui mollis dolor accumsan porta ridiculus. Mattis ultricies iaculis ex massa netus elit dapibus. Auctor nunc molestie venenatis eros aenean. Pretium phasellus tristique morbi lacinia nullam orci ridiculus mi vestibulum. Elit erat varius non nascetur fusce amet. Maecenas imperdiet libero imperdiet semper turpis quis donec. Ultricies felis nunc imperdiet hendrerit mattis mollis, litora primis.

Et himenaeos dapibus enim senectus fusce per? Mus iaculis aliquam accumsan aliquam fermentum torquent vivamus. Integer mollis eleifend ipsum mattis egestas molestie consectetur. Inceptos tincidunt odio parturient, aptent purus at quisque viverra cras. Dui sem quam blandit convallis phasellus erat class. Habitasse varius elit elementum, dapibus consequat mauris hendrerit metus. Suspendisse viverra tempus habitasse habitasse; justo cursus tristique. Pellentesque eget fames arcu montes feugiat dapibus vel parturient sollicitudin.

Turpis mus semper phasellus a integer; nascetur sollicitudin mauris. Torquent adipiscing est massa nascetur faucibus ultrices. Magnis congue iaculis egestas rutrum sagittis tempor. Montes quis non felis, ullamcorper montes nec pretium ultrices. Ante mi convallis viverra est potenti massa tristique viverra. Morbi pulvinar ex ligula dignissim class aenean. Porttitor orci parturient aptent vehicula turpis eros arcu. Tempus ligula imperdiet eu elementum luctus. Purus lacus suscipit suspendisse facilisi bibendum natoque.

Nascetur velit aptent bibendum sollicitudin, laoreet habitasse fusce. Sagittis praesent felis morbi praesent ridiculus magna at ornare. Eu feugiat est natoque nibh integer. Curabitur arcu aenean ut, malesuada aenean torquent ultrices. Hendrerit mi pharetra elit auctor phasellus lacinia. Mauris maecenas parturient quam morbi turpis erat malesuada volutpat. Donec risus torquent sodales elit mi; vivamus montes.

Gravida felis lorem semper ipsum aliquet amet molestie. Nulla fermentum per litora; maximus phasellus donec. Dapibus accumsan mus fusce phasellus potenti. Adipiscing vulputate porttitor non interdum risus risus. Nascetur id viverra habitant placerat; sodales felis accumsan. Maximus congue arcu magna semper convallis luctus leo rutrum hendrerit. Nostra et leo tristique convallis nibh semper tincidunt.

Maecenas a augue augue placerat tempor. Laoreet habitant cras feugiat eleifend duis tempor. Donec felis tempor nostra litora risus feugiat montes cursus. Pulvinar erat lectus facilisis vestibulum, sapien dolor fermentum integer. Ornare massa malesuada feugiat curabitur parturient. Vehicula cursus auctor potenti diam suspendisse auctor dapibus aenean. Accumsan ac aliquam netus id donec arcu erat nulla.

Habitasse nisi himenaeos imperdiet montes, fringilla turpis maximus duis. Nunc lectus fringilla justo, nibh dapibus urna arcu. Adipiscing torquent dolor faucibus eros sit efficitur. Sem gravida tortor dis lectus inceptos dis nunc. Amet elit proin orci proin morbi nostra ornare. Praesent suscipit nullam velit; adipiscing senectus ullamcorper. Et urna vivamus, ex sed posuere egestas. Placerat dignissim pulvinar potenti tincidunt tempus, blandit potenti. Mattis dolor nunc nostra mus cras tincidunt platea aenean ac. Hac amet id curabitur euismod bibendum massa mi.

Ultrices molestie molestie at maecenas ultrices vitae. Nam torquent eleifend dui vitae ad. Mattis odio netus feugiat arcu himenaeos. Platea facilisis dolor eget consectetur dis aenean morbi convallis. Mus sociosqu laoreet vel facilisis consequat litora quisque. Ullamcorper taciti fusce rutrum dui taciti. Posuere senectus at arcu integer magnis venenatis maximus rutrum amet. Augue eget aenean habitasse dis nam in. Magnis quis per venenatis eros nulla elit litora maximus.

Orci neque augue auctor nostra nam efficitur pellentesque. Est aptent sociosqu sapien nam euismod neque scelerisque. Praesent id vestibulum lacus tortor lacinia interdum id. Proin hendrerit sem nunc ut; justo malesuada volutpat. Curabitur orci taciti condimentum est blandit velit vel amet. Fringilla ipsum tempus dignissim dui nascetur mauris nam dapibus. Dolor justo interdum metus augue lacinia efficitur ullamcorper. Pretium posuere sodales taciti neque congue lobortis congue. Facilisi efficitur felis ligula id fringilla commodo id laoreet ligula. Volutpat neque velit ligula dolor torquent id.

Sagittis etiam praesent quam facilisis etiam taciti ultricies vivamus facilisi. Leo augue orci faucibus curae posuere? Habitasse morbi nostra enim malesuada penatibus sapien enim facilisis? Consectetur arcu vitae posuere nascetur scelerisque. Dignissim commodo habitasse amet ad, auctor proin. Elementum diam rhoncus cursus id senectus velit ac. Vehicula porttitor porta sagittis vestibulum iaculis? Venenatis nibh vulputate morbi per auctor aliquet.

Eleifend maximus condimentum dui hac fringilla. Torquent congue turpis turpis magna magna nibh. Nascetur elit hendrerit conubia pretium quisque euismod. Mollis sollicitudin litora molestie lacinia torquent scelerisque. Quis est auctor phasellus sapien ridiculus eget mollis ac ex. Ut et tempor dictumst suspendisse; curabitur conubia neque congue. Suspendisse sit accumsan est ipsum elementum, montes penatibus. Egestas nisi facilisi elit porttitor eu justo enim. Nulla magnis taciti adipiscing mollis donec ante amet lectus.

Maecenas nam pharetra non fermentum nostra, litora primis massa integer. Tempor sagittis rhoncus ex laoreet libero nascetur ultricies sapien. Penatibus vulputate diam sem consequat elementum quam. In lacinia nunc scelerisque congue netus. Efficitur quisque maecenas luctus natoque ac elementum? Scelerisque magna ad facilisis adipiscing vel nam eleifend convallis suscipit. Tempor pretium tellus, ipsum netus accumsan tellus. Natoque hendrerit turpis ullamcorper vehicula semper erat libero. Nullam tempor maximus suspendisse velit aptent dictum ante nisi a.

Leo pulvinar sagittis est posuere; diam cubilia. Sapien justo varius vivamus mi viverra aliquet. Amet nec mauris lectus imperdiet fringilla risus parturient. Ridiculus eleifend potenti finibus ante bibendum malesuada. Varius tellus duis mollis eros ultricies ut sodales. Dignissim sed curae venenatis dis faucibus rutrum interdum habitant auctor! Duis malesuada nam fusce; at vivamus etiam. Tortor litora pretium lacus mollis lorem justo vel sociosqu.

Enim mi eleifend aliquam mauris ante consequat dictum urna risus. Nullam condimentum ornare ex penatibus himenaeos nibh pulvinar quisque. Per per habitasse id conubia, nisi curabitur. Lobortis felis vivamus ut venenatis nullam ullamcorper sem lacinia. Hendrerit in commodo risus potenti sagittis dis. Consequat elit aliquet nostra venenatis mauris metus curabitur. Duis vulputate posuere class, mattis urna pellentesque!

Eget finibus id tincidunt gravida turpis netus dolor tempor. Dictum quisque velit per neque libero et. Conubia porttitor urna eu dolor risus sagittis justo tristique volutpat. Sed adipiscing nam nisi sollicitudin elit ullamcorper mus in. Efficitur feugiat varius purus eu sollicitudin gravida morbi erat. Viverra quis nascetur laoreet blandit ex parturient viverra litora litora. Non montes purus phasellus maecenas, molestie quisque id.

Erat ornare nibh quisque cubilia magna bibendum. Ornare adipiscing litora facilisis mi fames. Nisl nascetur ipsum fusce purus leo erat montes conubia. Auctor metus parturient nam per phasellus interdum tempor. Justo odio pharetra tortor accumsan viverra porta tellus vitae consequat. Enim augue dictum montes dui nullam ultricies facilisis, curabitur sollicitudin. Primis fames curabitur eleifend finibus vulputate sollicitudin risus aenean ultricies. Et mi nisl justo molestie rutrum dignissim fermentum bibendum.

Turpis nulla ornare mi amet odio aliquam enim. Libero cras torquent vitae vestibulum malesuada vestibulum fermentum sapien. Hac ipsum purus dictum luctus, ad habitasse montes. Tempor eu tortor magna ornare placerat sollicitudin litora. Vehicula leo nisi torquent odio nibh sollicitudin vitae. Malesuada felis magna nibh porta torquent justo tincidunt fames netus.

Turpis nullam a tellus justo vel enim viverra. Potenti tempus ut tempus facilisi ut conubia quis neque. Ad dui habitant maximus bibendum mus conubia malesuada hendrerit nisl. Per lacinia nam lectus maximus parturient efficitur. Integer malesuada elit curabitur erat semper. Id cras rhoncus id lorem hendrerit condimentum suspendisse. Mattis proin et at ac tempor hendrerit.

Tempor habitant mattis, neque netus ligula diam platea magnis. Feugiat at nibh taciti ridiculus conubia ac finibus erat. Ac pulvinar malesuada montes dictum montes in. Quisque ex molestie inceptos nascetur; erat gravida donec imperdiet. Dignissim sodales commodo ante iaculis urna nisi quis. Elementum rhoncus sollicitudin gravida litora convallis fringilla aenean nec. Rutrum inceptos facilisi quis dui donec. Hac non nascetur facilisi a viverra. Sapien morbi pharetra felis himenaeos iaculis sapien praesent.

Maecenas massa ac accumsan inceptos maximus nec ullamcorper risus pharetra. Quisque curabitur turpis maecenas eros, vitae nullam duis libero. Morbi amet quisque vulputate; interdum erat sagittis. Elementum tellus porttitor consequat gravida habitasse aenean. Phasellus aliquet aenean nisl suspendisse nascetur. Felis ex class sed pellentesque ornare pharetra. Consectetur faucibus tempor fringilla; varius potenti habitasse ultrices. Ullamcorper accumsan primis libero habitant; ligula faucibus cursus. Lacinia risus at torquent quisque fusce.

Dolor duis urna ante venenatis phasellus platea. Curabitur porta vivamus consequat nisi taciti phasellus nostra tincidunt. Vulputate augue commodo ut ultricies quisque consectetur accumsan. Magnis facilisis dictum curae eget fermentum porta diam orci. Scelerisque ullamcorper eget eleifend orci donec. Erat condimentum egestas habitasse praesent sociosqu semper conubia maecenas.

Efficitur placerat feugiat vitae dolor venenatis integer dictum donec. Nec in velit viverra cubilia suscipit fringilla quis potenti. Nullam mauris ex sociosqu consequat sem felis nisl efficitur turpis. Lacinia laoreet eros; venenatis ligula non justo mi sapien. Aenean habitasse eleifend consectetur vestibulum morbi scelerisque. Curabitur bibendum taciti eu nisl risus posuere volutpat dolor. Tempor risus tortor hendrerit sed suscipit commodo.

Sagittis etiam magna magna consequat litora adipiscing viverra lorem. Ullamcorper nibh vel torquent maximus ipsum dapibus nostra quisque cubilia. Urna purus nascetur arcu bibendum euismod. Nibh vel porttitor lacus orci vitae. Donec morbi amet ultrices posuere porta elementum. Lectus finibus hac augue congue phasellus metus. Iaculis litora curae sociosqu morbi; efficitur est libero. Diam hac hac morbi penatibus vestibulum tristique magna. Scelerisque nostra vitae eu, fermentum dui eget fusce.

Ac accumsan efficitur torquent curabitur proin vehicula. Elementum nunc ornare ipsum nisi ut nulla sodales. Nec ornare duis potenti curabitur vivamus? Luctus rutrum convallis eleifend ligula leo curae. Rutrum eros ornare dictum conubia gravida pharetra. Facilisi imperdiet ante dignissim tincidunt torquent vivamus vestibulum. Vivamus faucibus ridiculus, mi fusce augue auctor.

Dis placerat eros hendrerit montes finibus cubilia. Sagittis duis molestie sed neque cubilia consequat mi. Mus fringilla molestie habitasse malesuada maximus quis ante ligula. Dolor maecenas accumsan duis diam vestibulum. Velit lacus dis fames nibh pellentesque. Cursus habitant gravida mattis gravida tellus. Sed taciti class sem class nisi sit.

Dui sem nec cursus viverra ad per amet laoreet nulla? Id luctus natoque maecenas cras luctus massa mollis egestas interdum. Taciti augue tortor curae velit maecenas mauris. Himenaeos venenatis quam facilisi leo non purus. Faucibus vehicula tempus lacus fermentum at. Cras sagittis felis elit sit suspendisse tellus tortor nunc. Blandit condimentum nullam cras in purus quis cras aptent. Aliquam maecenas orci lacus vivamus nullam himenaeos. Ut vitae sociosqu malesuada consequat vel.

Montes auctor massa sodales mauris a. Ridiculus gravida lectus purus, hac curabitur congue. Enim vulputate penatibus taciti et platea sollicitudin egestas turpis. Phasellus litora imperdiet velit et per molestie. Rutrum id nam duis dis luctus amet quisque eros mi. Elit pretium nam at pellentesque et eleifend donec mattis netus. Litora nulla mollis viverra nec quis mattis metus torquent! Accumsan porttitor egestas sit aliquam lorem sagittis sociosqu finibus. Blandit adipiscing vel eu ultrices condimentum.
EOF
chown pete:penguingroup "$TEXT1"

MYCOOLFILE='/home/pete/mycoolfile'
cat << EOF > "$MYCOOLFILE"
         _nnnn_                      
        dGGGGMMb     ,"""""""""""""".
       @p~qp~~qMb    | Linux Rules! |
       M|@||@) M|   _;..............'
       @,----.JM| -'
      JS^\__/  qKL
     dZP        qKRb
    dZP          qKKb
   fZP            SMMb
   HZM            MMMM
   FqM            MMMM
 __| ".        |\dS"qML
 |    \`.       | \`' \Zq
_)      \.___.,|     .'
\____   )MMMMMM|   .'
     \`-'       \`--' hjm
EOF
chown pete:penguingroup "$MYCOOLFILE"

PUMPKIN='/home/pete/Downloads/Pumpkin'
mkdir "$PUMPKIN"
touch "$PUMPKIN/Buttercup" "$PUMPKIN/Turban" "$PUMPKIN/Butternut" "$PUMPKIN/Muscat"
chown --recursive pete:penguingroup "$PUMPKIN"

OLDFILE='/home/pete/Documents/oldfile'
cat << EOF > "$OLDFILE"

                                 ,        ,
                                /(        )\`
                                \ \___   / |
                                /- _  \`-/  '
                               (/\/ \ \   /\
                               / /   | \`    \
                               O O   ) /    |
                               \`-^--'\`<     '
                   TM         (_.)  _  )   /
|  | |\  | ~|~ \ /             \`.___/\`    /
|  | | \ |  |   X                \`-----' /
\`__| |  \| _|_ / \  <----.     __ / __   \
                    <----|====O)))==) \) /====
                    <----'    \`--' \`.__,' \
                                 |        |
                                  \       /
                             ______( (_  / \______
                           ,'  ,-----'   |        \
                           \`--{__________)        \/
	Kevin Woods
EOF
chown pete:penguingroup "$OLDFILE"

FILE1='/home/pete/Documents/file1'
cat << EOF > "$FILE1"
I'd just like to interject for a moment.  What you're referring to as Linux,
is in fact, GNU/Linux, or as I've recently taken to calling it, GNU plus Linux.
Linux is not an operating system unto itself, but rather another free component
of a fully functioning GNU system made useful by the GNU corelibs, shell
utilities and vital system components comprising a full OS as defined by POSIX.

Many computer users run a modified version of the GNU system every day,
without realizing it.  Through a peculiar turn of events, the version of GNU
which is widely used today is often called "Linux", and many of its users are
not aware that it is basically the GNU system, developed by the GNU Project.

There really is a Linux, and these people are using it, but it is just a
part of the system they use.  Linux is the kernel: the program in the system
that allocates the machine's resources to the other programs that you run.
The kernel is an essential part of an operating system, but useless by itself;
it can only function in the context of a complete operating system.  Linux is
normally used in combination with the GNU operating system: the whole system
is basically GNU with Linux added, or GNU/Linux.  All the so-called "Linux"
distributions are really distributions of GNU/Linux.
EOF
chown pete:penguingroup "$FILE1"

FILE2='/home/pete/Documents/file2'
cat << EOF > "$FILE2"
No, Richard, it's 'Linux', not 'GNU/Linux'. The most important contributions that the FSF made to Linux were the creation of the GPL and the GCC compiler. Those are fine and inspired products. GCC is a monumental achievement and has earned you, RMS, and the Free Software Foundation countless kudos and much appreciation.

Following are some reasons for you to mull over, including some already answered in your FAQ.

One guy, Linus Torvalds, used GCC to make his operating system (yes, Linux is an OS -- more on this later). He named it 'Linux' with a little help from his friends. Why doesn't he call it GNU/Linux? Because he wrote it, with more help from his friends, not you. You named your stuff, I named my stuff -- including the software I wrote using GCC -- and Linus named his stuff. The proper name is Linux because Linus Torvalds says so. Linus has spoken. Accept his authority. To do otherwise is to become a nag. You don't want to be known as a nag, do you?

(An operating system) != (a distribution). Linux is an operating system. By my definition, an operating system is that software which provides and limits access to hardware resources on a computer. That definition applies whereever you see Linux in use. However, Linux is usually distributed with a collection of utilities and applications to make it easily configurable as a desktop system, a server, a development box, or a graphics workstation, or whatever the user needs. In such a configuration, we have a Linux (based) distribution. Therein lies your strongest argument for the unwieldy title 'GNU/Linux' (when said bundled software is largely from the FSF). Go bug the distribution makers on that one. Take your beef to Red Hat, Mandrake, and Slackware. At least there you have an argument. Linux alone is an operating system that can be used in various applications without any GNU software whatsoever. Embedded applications come to mind as an obvious example.


You seem to like the lines-of-code metric. There are many lines of GNU code in a typical Linux distribution. You seem to suggest that (more LOC) == (more important). However, I submit to you that raw LOC numbers do not directly correlate with importance. I would suggest that clock cycles spent on code is a better metric. For example, if my system spends 90% of its time executing XFree86 code, XFree86 is probably the single most important collection of code on my system. Even if I loaded ten times as many lines of useless bloatware on my system and I never excuted that bloatware, it certainly isn't more important code than XFree86. Obviously, this metric isn't perfect either, but LOC really, really sucks. Please refrain from using it ever again in supporting any argument.

Last, I'd like to point out that we Linux and GNU users shouldn't be fighting among ourselves over naming other people's software. But what the heck, I'm in a bad mood now. I think I'm feeling sufficiently obnoxious to make the point that GCC is so very famous and, yes, so very useful only because Linux was developed. In a show of proper respect and gratitude, shouldn't you and everyone refer to GCC as 'the Linux compiler'? Or at least, 'Linux GCC'? Seriously, where would your masterpiece be without Linux? Languishing with the HURD?

If there is a moral buried in this rant, maybe it is this:

Be grateful for your abilities and your incredible success and your considerable fame. Continue to use that success and fame for good, not evil. Also, be especially grateful for Linux' huge contribution to that success. You, RMS, the Free Software Foundation, and GNU software have reached their current high profiles largely on the back of Linux. You have changed the world. Now, go forth and don't be a nag.

Thanks for listening.
EOF
chown pete:penguingroup "$FILE2"

FILE_1='/home/pete/Documents/file_1'
cat << EOF > "$FILE_1"
QWERTYUIOP{}|
ASDFGHJKL:"
ZXCVBNM<>?
EOF
chown pete:penguingroup "$FILE_1"

FILE_2='/home/pete/Documents/file_2'
cat << EOF > "$FILE_2"
"<>PYFGCRL?+|
AOEUIDHTNS_
:QJKXBMWZ
EOF
chown pete:penguingroup "$FILE_2"

