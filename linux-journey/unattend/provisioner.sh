#!/usr/bin/env bash

# post-installation this script is loaded by Packer using sftp/scp, then when this exits with status 0 Packer stops the machine and exports it

set -eou pipefail
set -x # added for debugging purposes

# boilerplate
[ "$(id -u)" -ne 0 ] && printf 'Provision script must be run as root\n' && exit 1
! command -v sed > /dev/null 2>&1 && printf "Command 'sed' not found, install it and try again\n" && exit 1
! command -v wget > /dev/null 2>&1 && printf "Command 'wget' not found, install it and try again\n" && exit 1
! command -v tee > /dev/null 2>&1 && printf "Command 'tee' not found, install it and try again\n" && exit 1

# stop potentially interferring apt processes
[ -d /etc/apt/apt.conf.d/ ] && printf 'APT::Periodic::Enable "0";\n' >> /etc/apt/apt.conf.d/10periodic
systemctl --quiet is-active apt-daily.timer && systemctl stop apt-daily.timer
systemctl --quiet is-active apt-daily.service && systemctl stop apt-daily.service
systemctl --quiet is-active apt-daily-upgrade.timer && systemctl stop apt-daily-upgrade.timer
systemctl --quiet is-active apt-daily-upgrade.service && systemctl stop apt-daily-upgrade.service

# remove cdrom as a package source
sed -i '/cdrom:/s/^/# /' /etc/apt/sources.list

# update, upgrade and auoremove
export DEBIAN_FRONTEND=nointeractive
export DEBCONF_NONINTERACTIVE_SEEN=true
apt-get update --quiet
apt-get full-upgrade --assume-yes --quiet --option Dpkg::Options::="--force-confnew"
apt-get autoremove --assume-yes --quiet
apt-get autoclean --assume-yes --quiet

# permit previously potentially interferring apt processes to start again
[ -d /etc/apt/apt.conf.d/ ] && rm /etc/apt/apt.conf.d/10periodic

# vagrant requires `hv_get_dhcp_info.sh` and `hv_get_dns_info.sh` which for some reason are missing from hyperv-daemons
mkdir -p /usr/libexec/hypervkvpd/
wget --quiet 'https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/plain/tools/hv/hv_get_dns_info.sh' \
	--output-document /usr/libexec/hypervkvpd/hv_get_dns_info
wget --quiet 'https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/plain/tools/hv/hv_get_dhcp_info.sh' \
	--output-document /usr/libexec/hypervkvpd/hv_get_dhcp_info
chmod 0755 /usr/libexec/hypervkvpd/hv_get_dns_info /usr/libexec/hypervkvpd/hv_get_dhcp_info

# configure default editor and pager
EDITOR="$(command -v nano)"
PAGER="$(command -v less)"
printf 'export EDITOR=%s\nexport PAGER=%s\n' "$EDITOR" "$PAGER" >> /etc/profile
update-alternatives --install /usr/bin/editor editor "$EDITOR" 50
update-alternatives --set editor "$EDITOR"
update-alternatives --install /usr/bin/pager pager "$PAGER" 50
update-alternatives --set pager "$PAGER"

# insert insecure well-known vagrant ssh keys
umask 0077
mkdir -p /root/.ssh/ /home/vagrant/.ssh/
wget --quiet 'https://raw.githubusercontent.com/hashicorp/vagrant/main/keys/vagrant.pub' --output-document - | \
	tee --append /root/.ssh/authorized_keys /home/vagrant/.ssh/authorized_keys
chmod 0600 /root/.ssh/authorized_keys /home/vagrant/.ssh/authorized_keys
chown -R root:root /root/.ssh/
chown -R vagrant:vagrant /home/vagrant/.ssh/

# generalise: remove current random seed, a new seed is generated at boot
systemctl --quiet is-active systemd-random-seed.service && systemctl stop systemd-random-seed.service
[ -f /var/lib/systemd/random-seed ] && rm /var/lib/systemd/random-seed

# generalise: re-generate machine-id and sshd-keys at first boot (note: ssh.service, not sshd.service)
systemctl disable ssh.service
printf '
@reboot root [ -f /etc/machine-id ] && rm /etc/machine-id && systemd-machine-id-setup
@reboot root rm /etc/ssh/ssh_host_* && ssh-keygen -A && systemctl enable --now ssh.service
@reboot root sleep 10 && rm /etc/cron.d/vagrant
' > /etc/cron.d/vagrant

